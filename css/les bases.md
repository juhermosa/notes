# Qu'est ce que le CSS ?

C'est un langage de feuille de style, c'est-à-dire qu'il permet d'appliquer
des styles sur différents éléments sélectionnés dans un document HTML.

## Commande CSS

`background color` = permet de définir la couleur utilisée pour l'arrière-plan d'un élément

`color` = couleur du texte par exmeple

`padding` = qui permet de définir les différents écarts de remplissage sur les quatre côtés d'un élément

`border-bottom` = bordure du côté bas d'un élément

`margin-bottom` = marge basse appliquée à un élément

`width` = largeur de la boite du contenu

`border` = définir une bordure

`transforme:scale 1.5` = faire un zoom sur un élément

`overflow` = il définit comment gérer le dépassement du contenu d'un élément dans son bloc

`display` = définit le type d'affichage utilisée pour le rendu d'un élément

`font size` = taille d'un texte

`text-align`= permet d'aligner le texte

`border-radius`=  permet de définir des coins arrondis pour la bordure d'un élément

`justify-content`= indique la façon dont l'espace doit être réparti entre et autour des éléments

`height`= définit la hauteur de la boîte de contenu d'un élément

`width`=  largeur de la boite de contenu

