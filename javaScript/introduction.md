----------  Sommaire ----------
             
  - [Introduction javaScript](#introduction-javascript)
  - [Définition de javaScript](#dfinition-de-javascript)
  - [Intégrer du code javascript à une page html](#intgrer-du-code-javascript--une-page-html)
  - [Valeurs primitives](#valeurs-primitives)
  - [Les variables let et const](#les-variables-let-et-const)

# Introduction javaScript

## Définition de javaScript

```
JavaScript est un langage de programmation quipermet d’implémenter 
des mécanismes complexes sur une page web. 
À chaque fois qu’une page web fait plus que simplement afficher du contenu
statique — afficher du contenu mis à jour à des temps déterminés, des cartes
interactives, des animations 2D/3D, des menus vidéo 
défilants, etc... — JavaScript a de bonnes chances d’être
impliqué. C’est la troisième couche des technologies
standards du web, les deux premières (HTML et CSS).
```

## Intégrer du code javascript à une page html

`script src="index.js"></script>`

`console.log`('coucou');

## Valeurs primitives

- On appelle valeur primitive en JavaScript une valeur
qui n'est pas un objet et qui ne peut pas être modifiée.


`numbers`  est utilisé pour manipuler les nombres comme des objets

`strings` est un constructeur de chaînes de caractères.

`booleans` est un type de données logique qui ne peut 
prendre que deux valeurs : true (vrai) ou false (faux).

`undefined` une valeur qui n'existe pas

`null` représentant la nullité au sens
où aucune valeur pour l'objet n'est présente.

`NaN` est une valeur utilisée pour représenter une quantité qui
n'est pas un nombre (Not a Number en anglais).

## Les variables let et const

`let` = L'instruction let permet de déclarer une variable dont la 
portée est celle du bloc courant, éventuellement en initialisant 
sa valeur.

`const` = La déclaration const permet de créer une constante
nommée accessible uniquement en lecture. Cela ne signifie
pas que la valeur contenue est immuable, uniquement
que l'identifiant ne peut pas être réaffecté. Autrement dit la valeur d'une constante ne peut pas être modifiée par des réaffectations ultérieures. Une constante ne peut pas être déclarée à nouveau.
          


