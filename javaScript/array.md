 ----------  Sommaire ----------
             
  - [Définition d'un Array](#dfinition-dun-array)
  - [Comment afficher l'Array](#comment-afficher-larray)
  - [Créer un tableau](#crer-un-tableau)
  - [forEach](#foreach)
  - [splice](#splice)
  - [slice](#slice)
  - [function](#function)
  - [sort](#sort)
  - [concat](#concat)
  - [filter](#filter)
  - [callback](#callback)
  - [push](#push)
  - [reduce](#reduce)
  - [map](#map)
  - [length](#length)
  
     
# Définition d'un Array
``` 
L'objet global Array est utilisé pour créer des tableaux.
```
![tab](../image/Capture%20tab.PNG)


## Comment afficher l'Array

`console.log` 

*Affiche un message dans la Console Web*

`console.table` 

*Affiche des données tabulaires sous la forme d'un tableau*

## Créer un tableau

```javascript
let fruits = ['orange','kiwi'];
console.table(fruits);
```

![tab](../image/Capture%20créer%20un%20tableau.PNG)

## forEach

- La méthode `forEach()` permet d'exécuter une fonction donnée sur chaque élément du tableau.

```javascript
const array1 = ['a', 'b', 'c'];

array1.forEach(element => console.log(element));
```

## splice

- La méthode `splice()` modifie le contenu d'un tableau en retirant des éléments
 et/ou en ajoutant de nouveaux éléments à même le tableau.
 On peut ainsi vider ou remplacer une partie d'un tableau.

```javascript
const fuits = ['banane', 'kiwi', 'orange', 'poire'];
months.splice(1, 0, 'ananas');
console.log(fruits);
```

- On ajoute l'`ananas` dans le tableaux.

```
months.splice(4, 1, 'fraise');
console.log(months);
```

- On retire le 4ème éléments (`poire`) et on ajoute `fraise`.

## slice

- La méthode slice() renvoie un objet tableau, contenant une copie superficielle (shallow copy)
d'une portion du tableau d'origine, la portion
est définie par un indice de début
et un indice de fin (exclus). Le tableau original
ne sera pas modifié.

```javascript
const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

console.log(animals.slice(2));
 = ["camel", "duck", "elephant"]

console.log(animals.slice(2, 4));
= ["camel", "duck"]

console.log(animals.slice(1, 5));
=  ["bison", "camel", "duck", "elephant"]
```

## Function
- Le constructeur Function crée un nouvel objet Function. En JavaScript, chaque fonction est un objet Function.

Appeler ce constructeur permet de créer des fonctions dynamiquement mais cette méthode souffre de défauts équivalents
à eval en termes de sécurité et de performance. Toutefois, à la différence d'eval, le constructeur Function permet
d'exécuter du code dans la portée globale.

```javascript
const sum = new Function('a', 'b', 'return a + b');

console.log(sum(2, 6));
```
## sort

- La méthode sort() trie les éléments d'un tableau, dans ce même tableau, et renvoie le tableau. Par défaut, le tri s'effectue sur les éléments du tableau convertis en chaînes de caractères et triées selon
les valeurs des unités de code UTF-16 des caractères.
 
```javascript
const months = ['March', 'Jan', 'Feb', 'Dec'];
months.sort();
console.log(months);
= ["Dec", "Feb", "Jan", "March"]

const array1 = [1, 30, 4, 21, 100000];
array1.sort();
console.log(array1);
=[1, 100000, 21, 30, 4]
```

## concat

- La méthode concat() est utilisée afin de fusionner un ou plusieurs tableaux en les concaténant. Cette méthode ne modifie pas les tableaux existants,
elle renvoie un nouveau tableau qui est le résultat de l'opération.

```javascript
const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2);

console.log(array3);
= ["a", "b", "c", "d", "e", "f"]
```

## filter

- La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine
qui remplissent une condition déterminée
 par la fonction callback.

```javascript
const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
 
 const result = words.filter(word => word.length > 6);
 
 console.log(result);
 = ["exuberant", "destruction", "present"]
```
 
## callback

- Une fonction de rappel (aussi appelée callback en anglais)
est une fonction passée dans une autre fonction en tant 
qu'argument, qui est ensuite invoquée à l'intérieur de la
fonction externe pour accomplir une sorte de routine ou 
d'action.

```javascript
function hello(name) {
  alert('Bonjour ' + name);
}

function processUserInput(callback) {
  var name = prompt('Entrez votre nom.');
  callback(name);
}

processUserInput(hello);
```


## push

- La méthode push() ajoute un ou plusieurs éléments à la fin
 d'un tableau et retourne la nouvelle taille du tableau.

```javascript
 const animals = ['pigs', 'goats', 'sheep'];
 
 const count = animals.push('cows');
 console.log(count);
= 4
 console.log(animals);
= ["pigs", "goats", "sheep", "cows"]
 
 animals.push('chickens', 'cats', 'dogs');
 console.log(animals);
= ["pigs", "goats", "sheep", "cows", "chickens", "cats", "dogs"]
```

## reduce

- La méthode reduce() applique une fonction qui est 
un « accumulateur » et qui traite chaque valeur d'une liste
(de la gauche vers la droite) afin de la réduire
à une seule valeur.

```javascript
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

console.log(array1.reduce(reducer));
=10

console.log(array1.reduce(reducer, 5));
=15
```
## map

- La méthode map() crée un nouveau tableau avec les résultats
de l'appel d'une fonction fournie sur chaque élément
du tableau appelant.

```javascript
const array1 = [1, 4, 9, 16];

const map1 = array1.map(x => x * 2);

console.log(map1);
=[2, 8, 18, 32]
```

## length

- La propriété length représente la longueur d'une chaine
de caractères, exprimée en nombre
de points de code UTF-16. C'est une propriété accessible
en lecture seule.

```javascript
const clothing = ['shoes', 'shirts', 'socks', 'sweaters'];

console.log(clothing.length);
```


